version: '2'
services:
  coyo-frontend:
    image: coyoapp/coyo-frontend:${COYO_VERSION}
    environment:
      - "COYO_BACKEND_URL=https://${COYO_BACKEND_URL}"
      - "COYO_BACKEND_URL_STRATEGY=relative"
      - "VIRTUAL_HOST_WEIGHT=1"
      - "VIRTUAL_HOST=https://${COYO_FRONTEND_URL}, http://${COYO_FRONTEND_URL}"
      - "FORCE_SSL=TRUE"
      - "GZIP_COMPRESSION_TYPE=text/html text/css application/javascript"
    expose:
      - "80"

  coyo-backend:
    image: coyoapp/coyo-backend:${COYO_VERSION}
    environment:
      - "COYO_BACKEND_URL=https://${COYO_BACKEND_URL}"
      - "COYO_FRONTEND_URL=https://${COYO_FRONTEND_URL}"
      - "COYO_BASE_URL=https://${COYO_FRONTEND_URL}"
      - "COYO_JAVA_OPTS=${COYO_JAVA_OPTS}"
      - "COYO_DB_MAX_ACTIVE_CONNECTIONS=${COYO_DB_MAX_ACTIVE_CONNECTIONS}"
      - "COYO_SERVER_MAX_THREADS=${COYO_SERVER_MAX_THREADS}"
      - "COYO_MANAGEMENT_USER=${COYO_MANAGEMENT_USER}"
      - "COYO_MANAGEMENT_PASSWORD=${COYO_MANAGEMENT_PASSWORD}"
      - "COYO_TENANT_STRATEGY=single"
      - "COYO_HOME=/var/lib/coyo/data"
      - "COYO_DB_HOST=coyo-db"
      - "COYO_DB_PORT=5432"
      - "COYO_DB_NAME=${COYO_DB_NAME}"
      - "COYO_DB_USER=${COYO_DB_USER}"
      - "COYO_DB_PASSWORD=${COYO_DB_PASSWORD}"
      - "COYO_ES_HOST=coyo-es"
      - "COYO_ES_PORT=9300"
      - "GZIP_COMPRESSION_TYPE=application/json"
      - "COYO_MONGODB_HOST=coyo-mongo"
      - "COYO_MONGODB_PORT=27017"
      - "COYO_MQ_HOST=coyo-mq"
      - "COYO_MQ_PORT=61613"
      - "COYO_MQ_NATIVE_HOST=coyo-mq"
      - "COYO_MQ_NATIVE_PORT=5672"
      - "COYO_THEME_HOST=coyo-scss"
      - "COYO_THEME_PORT=3030"
      - "COYO_PROFILE=${COYO_PROFILE}"
      - "COYO_BOOTSTRAP=${COYO_BOOTSTRAP}"
      - "COYO_REDIS_HOST=coyo-redis"
      - "COYO_REDIS_PORT=6379"
      - "COYO_TIKA_HOST=coyo-tika"
      - "COYO_TIKA_PORT=9998"
      - "COYO_MAIL_FROM=${COYO_MAIL_FROM}"
      - "COYO_SESSION_COOKIE_NAME=${COYO_SESSION_COOKIE_NAME}"
      - "COYO_SESSION_REMEMBER_TIMEOUT=${COYO_SESSION_REMEMBER_TIMEOUT}"
      - "COYO_BACKEND_CLUSTER=${COYO_BACKEND_CLUSTER}"
      - "SPRING_MAIL_HOST=${COYO_MAIL_HOST}"
      - "SPRING_MAIL_USERNAME=${COYO_MAIL_USERNAME}"
      - "SPRING_MAIL_PASSWORD=${COYO_MAIL_PASSWORD}"
      - "SPRING_MAIL_PORT=${COYO_MAIL_PORT}"
      - "SPRING_MAIL_PROTOCOL=${COYO_MAIL_PROTOCOL}"
      - "VIRTUAL_HOST_WEIGHT=2"
      - "VIRTUAL_HOST=https://${COYO_BACKEND_URL}/api, https://${COYO_BACKEND_URL}/api/*, https://${COYO_BACKEND_URL}/web, https://${COYO_BACKEND_URL}/web/*, https://${COYO_BACKEND_URL}/tenants, https://${COYO_BACKEND_URL}/tenants/*, https://${COYO_BACKEND_URL}/manage/*, http://${COYO_BACKEND_URL}/api, http://${COYO_BACKEND_URL}/api/*, http://${COYO_BACKEND_URL}/web, http://${COYO_BACKEND_URL}/web/*, http://${COYO_BACKEND_URL}/tenants, http://${COYO_BACKEND_URL}/tenants/*, http://${COYO_BACKEND_URL}/manage/*"
    links:
      - coyo-db
      - coyo-mq
      - coyo-es
      - coyo-redis
      - coyo-scss
      - coyo-mongo
      - coyo-tika
      - coyo-elk
    volumes:
      - "${COYO_DATA_STORAGE}/backend:/var/lib/coyo/data"
    expose:
      - "8080"

  coyo-lb:
    image: "dockercloud/haproxy:1.5"
    environment:
      - "STATS_AUTH=${COYO_MANAGEMENT_USER}:${COYO_MANAGEMENT_PASSWORD}"
      - "TIMEOUT=connect 5000, client 300000, server 300000"
      - "CERT_FOLDER=/certs/"
    links:
      - coyo-frontend
      - coyo-backend
      - coyo-docs
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${COYO_CERT_FILE}:/certs/cert.pem
    ports:
      - "80:80"
      - "443:443"
      - "1936:1936"

  coyo-lb-internal:
    image: "dockercloud/haproxy:1.5"
    environment:
      - "HTTP_BASIC_AUTH=${COYO_MANAGEMENT_USER}:${COYO_MANAGEMENT_PASSWORD}"
      - "CERT_FOLDER=/certs/"
    links:
      - coyo-backup
      - coyo-elk
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${COYO_CERT_FILE}:/certs/cert.pem
    ports:
      - "5601:5601"
      - "8083:8083"

  coyo-db:
    image: coyoapp/coyo-db:${COYO_VERSION}
    environment:
      - "POSTGRES_DB=${COYO_DB_NAME}"
      - "POSTGRES_USER=${COYO_DB_USER}"
      - "POSTGRES_PASSWORD=${COYO_DB_PASSWORD}"
    volumes:
      - "${COYO_DATA_STORAGE}/db:/var/lib/postgresql/data"
    expose:
      - "5432"

  coyo-mq:
    image: coyoapp/coyo-mq:${COYO_VERSION}
    expose:
      - "4369"
      - "5671"
      - "5672"
      - "15671"
      - "15672"
      - "25672"
      - "61613"

  coyo-es:
    image: coyoapp/coyo-es:${COYO_VERSION}
    environment:
      - "ES_HEAP_SIZE=${ES_HEAP_SIZE}"
    volumes:
      - "${COYO_DATA_STORAGE}/es:/usr/share/elasticsearch/data"
    expose:
      - "9200"
      - "9300"

  coyo-docs:
    image: coyoapp/coyo-docs:${COYO_VERSION}
    environment:
      - "VIRTUAL_HOST=https://${COYO_FRONTEND_URL}/docs, https://${COYO_FRONTEND_URL}/docs/*, http://${COYO_FRONTEND_URL}/docs, http://${COYO_FRONTEND_URL}/docs/*"
      - "VIRTUAL_HOST_WEIGHT=3"
      - "FORCE_SSL=TRUE"

  coyo-elk:
    image: coyoapp/coyo-elk:${COYO_VERSION}
    environment:
      - "ELASTICSEARCH_START=0"
      - "EXCLUDE_PORTS=4560,5000,5044,9200,9300"
      - "VIRTUAL_HOST=https://${COYO_LOGGING_URL}"
    volumes:
      - "${COYO_DATA_STORAGE}/elk:/var/lib/elasticsearch"
    expose:
      - "4560"
      - "5000"
      - "5044"
      - "9200"
      - "9300"

  coyo-redis:
    image: coyoapp/coyo-redis:${COYO_VERSION}
    expose:
      - "6379"

  coyo-scss:
    image: coyoapp/coyo-scss:${COYO_VERSION}
    environment:
      - "COYO_THEME_PORT=3030"
    expose:
      - "3030"

  coyo-mongo:
    image: mongo:3.2
    volumes:
      - "${COYO_DATA_STORAGE}/mongo:/data/db"
    expose:
      - "27017"

  coyo-tika:
    image: coyoapp/coyo-tika:${COYO_VERSION}
    expose:
      - "9998"

  coyo-backup:
    image: coyoapp/coyo-backup:${COYO_VERSION}
    environment:
      - "COYO_BACKEND_URL=https://${COYO_BACKEND_URL}"
      - "BACKUP_CRON=${BACKUP_CRON}"
      - "BACKUP_PATH=${BACKUP_PATH}"
      - "COYO_DB_HOST=coyo-db"
      - "COYO_DB_PORT=5432"
      - "COYO_DB_NAME=${COYO_DB_NAME}"
      - "COYO_DB_USER=${COYO_DB_USER}"
      - "COYO_DB_PASSWORD=${COYO_DB_PASSWORD}"
      - "COYO_FILE_HOST=coyo-mongo"
      - "COYO_FILE_PORT=${COYO_FILE_PORT}"
      - "COYO_FILE_DATABASE=${COYO_FILE_DATABASE}"
      - "VIRTUAL_HOST=https://${COYO_BACKUP_URL}"
    links:
      - "coyo-backend"
      - "coyo-db"
      - "coyo-mongo"
    volumes:
      - "${COYO_DATA_STORAGE}/backup:${BACKUP_PATH}"
    expose:
      - "8083"
