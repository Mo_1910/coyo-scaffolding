(function () {
  'use strict';

  var login = require('../../login.page.js');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var BookmarkingWidget = require('./bookmarking-widget.page.js');
  var components = require('../../components.page');
  var testhelper = require('../../testhelper');

  describe('bookmarking widget', function () {
    var widgetSlot, widgetChooser, widget, bookmarkingWidget, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      bookmarkingWidget = new BookmarkingWidget(widget);

      widgetSlot.addButton.click();
      widgetChooser.selectByName('Bookmarks');
    });

    it ('should be created', function () {

      bookmarkingWidget.addBookmark.url.sendKeys('http://www.mindsmash.com');
      bookmarkingWidget.addBookmark.saveButton.click();
      bookmarkingWidget.addBookmark.title.sendKeys('Take a look at mindsmash!');
      bookmarkingWidget.addBookmark.saveButton.click();

      expect(bookmarkingWidget.editBookmark.title.getAttribute('value')).toBe('Take a look at mindsmash!');

      bookmarkingWidget.editBookmark.title.clear();
      bookmarkingWidget.editBookmark.title.sendKeys('Google');

      bookmarkingWidget.editBookmark.urlButton.click();
      bookmarkingWidget.editBookmark.url.clear();

      expect(bookmarkingWidget.editBookmark.bookmarkHasError()).toBe(true);

      bookmarkingWidget.editBookmark.url.sendKeys('https://www.google.de');

      expect(bookmarkingWidget.editBookmark.bookmarkHasError()).toBe(false);

      // save
      navigation.viewEditOptions.saveButton.click();
      expect(bookmarkingWidget.bookmark.getText()).toBe('Google');

      // remove widget
      navigation.editView();
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });
})();
