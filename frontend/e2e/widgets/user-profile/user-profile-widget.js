(function () {
  'use strict';

  var login = require('../../login.page.js');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page.js');
  var UserProfileWidget = require('./user-profile-widget.page.js');
  var components = require('../../components.page');
  var testhelper = require('../../testhelper');
  var ProfilePage = require('../../profile/profile.page.js');

  describe('user profile widget', function () {

    var widgetSlot, widgetChooser, userProfileWidget, navigation, firstname, lastname, email, jobTitle, department, phone;

    beforeEach(function () {
      login.loginDefaultUser();
      var profilePage = new ProfilePage();
      navigation = new Navigation();

      var key = Math.floor(Math.random() * 1000000);
      jobTitle = 'Tester';
      department = 'QA';
      phone = '+40 (0) 40 1234 5678';
      firstname = 'Max';
      lastname = 'Mustermann' + key;
      email = 'max.mustermann' + key + '@mindsmash.com';
      var password = 'Secret123';
      testhelper.createUser(firstname, lastname, email, password, ['User']);
      login.logout();
      login.login(email, password);

      navigation.profile.click();
      testhelper.cancelTour();
      profilePage.info.click();
      profilePage.work.editButton.click();

      profilePage.work.jobTitleInput.clear();
      profilePage.work.jobTitleInput.sendKeys(jobTitle);
      profilePage.work.departmentInput.clear();
      profilePage.work.departmentInput.sendKeys(department);
      profilePage.work.submitButton.click();

      profilePage.contact.editButton.click();
      profilePage.contact.phoneInput.clear();
      profilePage.contact.phoneInput.sendKeys(phone);
      profilePage.contact.submitButton.click();

      login.logout();
      login.loginDefaultUser();

      testhelper.createAndOpenPage('testpage' + key);
      navigation.editView();
    });

    it('create a new user-profile widget', function () {
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      widgetChooser = widgetSlot.widgetChooser;
      userProfileWidget = new UserProfileWidget(widgetSlot.getWidget(0));

      widgetSlot.addButton.click();
      widgetChooser.selectByName('User Profile');

      userProfileWidget.settings.userChooser.openButton.click();
      userProfileWidget.settings.userChooser.modal.tabs.users.heading.click();
      userProfileWidget.settings.userChooser.modal.tabs.users.filterField.sendKeys(lastname);
      userProfileWidget.settings.userChooser.modal.tabs.users.filterResult.click();
      userProfileWidget.settings.userChooser.modal.selectButton.click();
      widgetChooser.saveButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(userProfileWidget.renderedUserProfile.name.getText()).toBe(firstname + ' ' + lastname);
      expect(userProfileWidget.renderedUserProfile.jobTitle.getText()).toBe(jobTitle);

      // edit widget
      navigation.editView();
      browser.actions().mouseMove(userProfileWidget.container).perform();
      widgetSlot.editWidgetButton.click();
      userProfileWidget.settings.showInfo.click();
      widgetChooser.saveButton.click();
      navigation.viewEditOptions.saveButton.click();

      expect(userProfileWidget.renderedUserProfile.name.getText()).toBe(firstname + ' ' + lastname);
      expect(userProfileWidget.renderedUserProfile.jobTitle.getText()).toBe(jobTitle);
      expect(userProfileWidget.renderedUserProfile.email.getText()).toBe(email);
      expect(userProfileWidget.renderedUserProfile.department.getText()).toBe(department);
      expect(userProfileWidget.renderedUserProfile.phone.getText()).toBe('+40 (0) 40 1234 5678');

      // remove widget
      navigation.editView();
      userProfileWidget.hover();
      userProfileWidget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);
    });
  });
})();
