(function () {
  'use strict';

  var login = require('../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var SubscriptionsWidget = require('./subscriptions-widget.page');
  var testhelper = require('../../testhelper');

  describe('subscriptions widget', function () {
    var widgetSlot, widgetChooser, widget, navigation, pageName, targetPageName;

    beforeEach(function () {
      var key = Math.floor(Math.random() * 1000000);
      navigation = new Navigation();

      login.loginDefaultUser();

      // create user
      var username = 'max' + key + '@mindsmash.com';
      var password = 'Secret123';
      testhelper.createUser('Max', 'Mustermann' + key, username, password, ['Admin', 'User']);
      login.logout();
      login.login(username, password);

      targetPageName = 'testpage-target-' + key;
      pageName = 'testpage-context-' + key;
      testhelper.createAndOpenPage(targetPageName);
      testhelper.createAndOpenPage(pageName);
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = new SubscriptionsWidget(widgetSlot.getWidget(0));

    });

    it('should be created', function () {
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Subscriptions');
      navigation.viewEditOptions.saveButton.click();

      var pages = widget.renderedWidget.pages;
      expect(pages.count()).toBe(3);
      expect(pages.get(2).getText()).toBe(targetPageName);
      pages.get(2).click();
      expect(browser.getCurrentUrl()).toMatch('pages/' + targetPageName);
    });
  });
})();
