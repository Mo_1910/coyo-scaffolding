(function () {
  'use strict';

  var extend = require('util')._extend;
  function ButtonWidget(widget) {
    var api = extend(this, widget);

    api.button = api.container.$('a[ng-class="$ctrl.widget.settings._button.btnClass"]');
    api.input = api.button.$('input[type="text"]');
    api.buttonHasClass = function (button, cls) {
      return button.getAttribute('class').then(function (classes) {
        return classes.split(' ').indexOf(cls) !== -1;
      });
    };

    api.settings = {
      text: $('[ng-model="$ctrl.model.settings.text"]'),
      url: $('[ng-if="$ctrl.linkPattern"]'),
      severityToggle: element(by.model('$ctrl.model.settings._button')),
      severityOption: function (label) {
        return api.settings.severityToggle.element(by.cssContainingText('.ui-select-choices-row-inner div', label));
      },
      preview: $('[ng-class="$ctrl.model.settings._button.btnClass"]')
    };

    api.inlineOptions = {
      severityToggle: widget.container.$('.widget-options span.btn-dark'),
      severityOption: function (label) {
        return widget.container.element(by.cssContainingText('.widget-options a', label));
      }
    };
  }
  module.exports = ButtonWidget;
})();
