(function () {
  'use strict';

  var pageIds = [];
  var userIds = [];
  var timelineItemsIds = [];
  var timelineIds = [];

  function createUser(firstname, lastname, email, password, roles) {
    browser.executeAsyncScript(function (firstname, lastname, email, password, roles, callback) {
      function loadUser(roleIds) {
        return new UserModel({
          active: true,
          superadmin: false,
          firstname: firstname,
          lastname: lastname,
          email: email,
          password: password,
          roleIds: roleIds,
          groupIds: []
        }).save().then(function (user) {
          callback({userId: user.id});
        }).catch(function (error) {
          callback(error);
        });
      }

      var injector = $('body').injector();
      var RoleModel = injector.get('RoleModel');
      var UserModel = injector.get('UserModel');
      if (roles && roles.length > 0) {
        return RoleModel.query().then(function (result) {
          var roleIds = _.filter(result.content, function (role) {
            return roles.indexOf(role.displayName) >= 0;
          }).map(function (role) {
            return role.id;
          });
          return loadUser(roleIds);
        }).catch(function (error) {
          callback(error);
        });
      } else {
        return loadUser([]);
      }
    }, firstname, lastname, email, password, roles).then(function (result) {
      if (result.error) {
        fail('Creating user account failed: ' + JSON.stringify(result.error));
      }
      userIds.push(result.userId);
    });
  }

  /**
   * Delete all users created by createUser via direct AJAX call.
   */
  function deleteUsers() {
    userIds.forEach(function (userId) {
      browser.executeAsyncScript(function (userId, callback) {
        var injector = $('body').injector();
        var UserModel = injector.get('UserModel');
        new UserModel({id: userId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, userId).then(function (error) {
        if (error) {
          console.log('Deleting user failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    userIds = [];
  }

  /**
   * Helper function to create and open a basic page without having to use the UI.
   * This is intended to be used in tests that need a clean page environment to work in
   * but don't care about the page or how it is created (esp. app + widget tests).
   *
   * @param {string} name The name of the page
   * @returns {object} promise to be resolved when the script is finished
   */
  function createAndOpenPage(name) {
    return createPage(name).then(function (pageId) {
      browser.get('/pages/' + name);
      disableAnimations();
      cancelTour();
      return pageId;
    });
  }

  /**
   * Helper function to create a basic page without having to use the UI.
   * This is intended to be used in tests that need a clean page environment to work in
   * but don't care about the page or how it is created (esp. app + widget tests).
   *
   * @param {string} name The name of the page
   * @returns {object} promise to be resolved when the script is finished
   */
  function createPage(name) {
    return browser.executeAsyncScript(function (name, callback) {
      var injector = $('body').injector();
      var PageModel = injector.get('PageModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        return new PageModel({
          name: name,
          visibility: 'PUBLIC',
          adminIds: [user.id],
          adminGroupIds: [],
          memberGroupIds: [],
          memberIds: [],
          autoSubscribe: false,
          autoSubscribeType: 'NONE',
          autoSubscribeUserIds: [],
          autoSubscribeGroupIds: []
        }).save().then(function (page) {
          callback({pageId: page.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, name).then(function (result) {
      if (result.error) {
        fail('Creating page failed: ' + JSON.stringify(result.error));
      }
      pageIds.push(result.pageId);
      return result.pageId;
    });
  }


  /**
   * Delete all pages created by createAndOpenPage via direct AJAX call.
   */
  function deletePages() {
    pageIds.forEach(function (pageId) {
      browser.executeAsyncScript(function (pageId, callback) {
        var injector = $('body').injector();
        var PageModel = injector.get('PageModel');
        new PageModel({id: pageId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, pageId).then(function (error) {
        if (error) {
          console.log('Deleting page failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    pageIds = [];
  }

  function hasTestPages() {
    return pageIds.length > 0;
  }

  /**
   * Helper function to create a timeline for a sender
   *
   * @param {string} senderId The id of a sender (e.g. page)
   * @param {string} timelineName The name of the timeline
   * @returns {string} promise timeline id
   */
  function createTimeline(senderId, timelineName) {
    return browser.executeAsyncScript(function (senderId, timelineName, callback) {
      var injector = $('body').injector();
      var AppModel = injector.get('AppModel');
      return new AppModel({
        senderId: senderId,
        key: 'timeline',
        name: timelineName,
        active: true,
        settings: null
      }).save().then(function (timeline) {
        callback({timelineId: timeline.id});
      }).catch(function (error) {
        callback({error: error});
      });
    }, senderId, timelineName).then(function (result) {
      if (result.error) {
        fail('Creating timeline failed: ' + JSON.stringify(result.error));
      }
      var timelineIdList = [];
      timelineIdList.push(result.timelineId);
      timelineIdList.push(senderId);
      timelineIds.push(timelineIdList);
      return result.timelineId;
    });
  }

  /**
   * Helper function to delete all timelines created by createTimeline
   */
  function deleteTimelines() {
    timelineIds.forEach(function (timelineIdList) {
      var timelineId = timelineIdList[0];
      var senderId = timelineIdList[1];
      browser.executeAsyncScript(function (timelineId, senderId, callback) {
        var injector = $('body').injector();
        var AppModel = injector.get('AppModel');
        new AppModel({
          id: timelineId,
          senderId: senderId
        }).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, timelineId, senderId).then(function (error) {
        if (error) {
          console.log('Deleting timeline failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    timelineIds = [];
  }

  /**
   * Helper function to create a timeline item
   *
   * @param {string} message message to be posted on timeline
   * @param {string} senderId recipient id for message
   * @returns {string} promise timelineitem id
   */
  function createTimelineEntry(message, senderId) {
    return browser.executeAsyncScript(function (message, senderId, callback) {
      var injector = $('body').injector();
      var TimelineItemModel = injector.get('TimelineItemModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        return new TimelineItemModel({
          authorId: user.id,
          data: {
            message: message
          },
          type: 'post',
          recipientId: senderId
        }).save().then(function (timelineitem) {
          callback({timelineitemId: timelineitem.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, message, senderId).then(function (result) {
      if (result.error) {
        fail('Creating timeline item failed: ' + JSON.stringify(result.error));
      }
      timelineItemsIds.push(result.timelineitemId);
      return result.timelineitemId;
    });
  }

  /**
   * Helper function to delete all timelineitems created by createTimelineEntry
   */
  function deleteTimelineEntry() {
    timelineItemsIds.forEach(function (timelineId) {
      browser.executeAsyncScript(function (timelineId, callback) {
        var injector = $('body').injector();
        var TimelineItemModel = injector.get('TimelineItemModel');
        new TimelineItemModel({id: timelineId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, timelineId).then(function (error) {
        if (error) {
          console.log('Deleting timelineItem failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    timelineItemsIds = [];
  }

  /**
   * protractor does not know about css3 animations as they are not part of the angular digest cycle and thus
   * aitForAngular does wait for animations to finish. As a workaround we simply disable all transitions for testing.
   */
  function disableAnimations() {
    var css = '* {' +
        '-webkit-transition: none !important;' +
        '-moz-transition: none !important;' +
        '-o-transition: none !important;' +
        '-ms-transition: none !important;' +
        'transition: none !important;' +
        'animation: none !important;' +
        '-webkit-animation: none !important' +
        '-moz-animation: none !important;' +
        '-o-animation: none !important;' +
        '-ms-animation: none !important;' +
        '}';
    var script = '$(\'<style type=\"text/css\">' + css + '</style>\').appendTo($(\'body\'));';
    browser.executeScript(script);
    browser.sleep(100);
  }

  /**
   * Cancel any tour popup if it is present, ignore otherwise
   */
  function cancelTour() {
    var tourClose = $('.tour-step-close');
    tourClose.isPresent().then(function (isPresent) {
      if (isPresent) {
        tourClose.click();
      }
    });
  }

  module.exports = {
    createUser: createUser,
    deleteUsers: deleteUsers,
    createAndOpenPage: createAndOpenPage,
    createPage: createPage,
    deletePages: deletePages,
    hasTestPages: hasTestPages,
    createTimeline: createTimeline,
    deleteTimelines: deleteTimelines,
    createTimelineEntry: createTimelineEntry,
    deleteTimelineEntry: deleteTimelineEntry,
    disableAnimations: disableAnimations,
    cancelTour: cancelTour
  };

})();
