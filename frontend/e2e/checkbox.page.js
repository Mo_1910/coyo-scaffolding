(function () {
  'use strict';

  function Checkbox(element) {
    var api = this;
    api.click = function () {
      element.click();
    };
    api.isChecked = function () {
      return element.getAttribute('class').then(function (clazz) {
        return /checked/.test(clazz);
      });
    };
  }

  module.exports = Checkbox;

})();
