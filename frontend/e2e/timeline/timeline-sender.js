(function () {
  'use strict';

  var login = require('../login.page.js');
  var testhelper = require('../testhelper.js');
  var SenderTimeline = require('./timeline-sender.page.js');
  var Navigation = require('../navigation.page.js');
  var PageList = require('../pages/page-list.page.js');
  var PageDetails = require('../pages/page-details.page.js');

  describe('sender timeline', function () {
    var senderTimeline, navigation, pageList, pageDetails;

    beforeEach(function () {
      senderTimeline = new SenderTimeline();
      navigation = new Navigation();
      pageList = new PageList();
      pageDetails = new PageDetails();
      login.loginDefaultUser();
    });

    afterEach(function () {
      testhelper.deleteTimelineEntry();
      testhelper.deleteTimelines();
      testhelper.deletePages();
    });

    it('load more on home timeline', function () {
      var pageName = 'MyFirstTestPage' + Math.floor(Math.random() * 1000000);
      var PAGE_SIZE = 8;
      // create page and timeline entries
      createPageAndTimelineItems(pageName, PAGE_SIZE);

      // refresh browser to mark timeline items as 'seen'
      browser.refresh();
      checkNumOfTimelineItems(PAGE_SIZE);
    });

    it('load more on page timeline', function () {
      var pageName = 'MySecondTestPage' + Math.floor(Math.random() * 1000000);
      var PAGE_SIZE = 8;
      // create page and timeline entries
      createPageAndTimelineItems(pageName, PAGE_SIZE);

      navigation.pages.click();
      testhelper.cancelTour();

      pageList.search(pageName).get(0).$('h4[ng-click="ctrl.openPage(page)"]').click();
      pageDetails.options.addApp.click();
      pageDetails.addApp.timeline.click();
      pageDetails.addApp.save.click();
      checkNumOfTimelineItems(PAGE_SIZE);

    });

    it('comment on item on page timeline', function () {
      var pageName = 'MyThirdTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();

          testhelper.createTimelineEntry('This is a new timelineentry', pageId).then(function () {
            senderTimeline.addComment('This is a text for a comment 1', 0);
            senderTimeline.timelineItem.submitBtn.click();
            senderTimeline.addComment('This is a text for a comment 2', 0);
            senderTimeline.timelineItem.submitBtn.click();

            testhelper.createTimelineEntry('This is a new timelineentry 123', pageId).then(function () {
              senderTimeline.addComment('This is a text for a comment 123 1', 0);
              senderTimeline.timelineItem.submitBtn.click();
              senderTimeline.addComment('This is a text for a comment 123 2', 0);
              senderTimeline.timelineItem.submitBtn.click();

              // a new timeline item will be located at the top of the timeline
              senderTimeline.timelineItem.commentMessage(0, 0).getText().then(function (text) {
                expect(text).toBe('This is a text for a comment 123 1');
              });
              senderTimeline.timelineItem.commentMessage(0, 1).getText().then(function (text) {
                expect(text).toBe('This is a text for a comment 123 2');
              });
              senderTimeline.timelineItem.commentMessage(1, 0).getText().then(function (text) {
                expect(text).toBe('This is a text for a comment 1');
              });
              senderTimeline.timelineItem.commentMessage(1, 1).getText().then(function (text) {
                expect(text).toBe('This is a text for a comment 2');
              });
            });
          });
        });
      });
    });

    it('like timelineItem', function () {
      var pageName = 'MyThirdTestPage' + Math.floor(Math.random() * 1000000);
      var timelineName = 'Timeline';
      testhelper.createAndOpenPage(pageName).then(function (pageId) {
        testhelper.createTimeline(pageId, timelineName).then(function () {
          browser.refresh();
          testhelper.createTimelineEntry('An Item to be liked', pageId).then(function () {
            senderTimeline.timelineItem.likeBtn.click();
            senderTimeline.timelineItem.likeBtnText.getText().then(function (text) {
              expect(text).toBe('–You like this');
            });
          });
        });
      });
    });

    function createPageAndTimelineItems(pageName, pagesize) {
      testhelper.createPage(pageName).then(function (pageId) {
        for (var i = 0; i < pagesize + pagesize; i++) {
          testhelper.createTimelineEntry('new entry for the timeline ' + i, pageId);
        }
      });
    }

    function checkNumOfTimelineItems(pagesize) {
      // check timeline items before loadMore()
      senderTimeline.timelineItems.count().then(function (num) {
        expect(num).toBe(pagesize);
      });

      // load more for timeline
      senderTimeline.loadMore.click();
      senderTimeline.timelineItems.count().then(function (num) {
        expect(num).toBe(pagesize + pagesize);
      });
    }

  });
})();
