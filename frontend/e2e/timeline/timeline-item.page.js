(function () {
  'use strict';

  function TimelineItem(container) {
    var api = this;

    api.container = container;
    api.authorName = api.container.$('.timeline-item-author-name');
    api.message = api.container.$('.timeline-item-message');
  }

  module.exports = TimelineItem;

})();
