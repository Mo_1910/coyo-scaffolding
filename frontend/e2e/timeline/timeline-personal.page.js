(function () {
  'use strict';

  function PersonalTimeline() {
    var api = this;

    api.form = {
      container: element(by.css('.timeline-form-inline')),
      messageField: element(by.css('.timeline-form-inline textarea[name=message]')),
      attachmentTrigger: element(by.css('.timeline-form-attachment-trigger')),
      submitBtn: element(by.css('.timeline-form-inline button[type=submit]'))
    };

    api.stream = element(by.css('.timeline-stream'));

    api.items = element.all(by.css('.timeline-item'));
  }

  module.exports = PersonalTimeline;

})();
