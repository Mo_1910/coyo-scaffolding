(function () {
  'use strict';

  var login = require('./../../login.page.js');
  var theme = require('./theme.page');

  describe('theme administration', function () {

    beforeEach(function () {
      login.loginDefaultUser();
      theme.get();
    });

    it('should set main background color', function () {
      theme.colors.mainBackground.clear();
      theme.colors.mainBackground.sendKeys('#000000');
      theme.saveButton.click();
      expect($('.container-admin').getCssValue('background-color')).toBe('rgba(0, 0, 0, 1)');
    });

    afterEach(function () {
      theme.get();
      theme.colors.mainBackground.clear();
      theme.saveButton.click();
    });
  });
})();
