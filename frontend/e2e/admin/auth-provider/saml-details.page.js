(function () {
  'use strict';

  var Select = require('../../select.page.js');

  module.exports = {
    generalTab: {
      heading: $('.uib-tab span[translate="ADMIN.AUTHENTICATION.SAML.TABS.HEADINGS.GENERAL"]'),
      entityId: element(by.model('$ctrl.ngModel.properties.entityId')),
      authenticationEndpoint: element(by.model('$ctrl.ngModel.properties.authenticationEndpoint')),
      logoutEndpoint: element(by.model('$ctrl.ngModel.properties.logoutEndpoint')),
      authenticationExpiryInSeconds: element(by.model('$ctrl.ngModel.properties.authenticationExpiryInSeconds')),
      userDirectory: new Select('$ctrl.ngModel.properties.userDirectory')
    },
    signRequestsTab: {
      heading: $('.uib-tab span[translate="ADMIN.AUTHENTICATION.SAML.TABS.HEADINGS.SIGN_REQUEST"]'),
      signRequests: element(by.model('$ctrl.ngModel.properties.signRequests')),
      signingCertificate: element(by.model('$ctrl.ngModel.properties.signingCertificate')),
      signingPrivateKey: element(by.model('$ctrl.ngModel.properties.signingPrivateKey')),
      signingPrivateKeyPassword: element(by.model('$ctrl.ngModel.properties.signingPrivateKeyPassword'))
    },
    validateResponseTab: {
      heading: $('.uib-tab span[translate="ADMIN.AUTHENTICATION.SAML.TABS.HEADINGS.VALIDATE_RESPONSE"]'),
      disableTrustCheck: element(by.model('$ctrl.ngModel.properties.disableTrustCheck'))
    }
  };

})();
