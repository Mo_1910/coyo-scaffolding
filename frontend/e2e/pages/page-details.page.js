(function () {
  'use strict';

  function PageDetails() {
    var api = this;

    api.title = element(by.css('.page .title'));

    var imageHeaderContainer = $('.image-header');
    api.imageHeader = {
      changeCover: imageHeaderContainer.$('.change-cover'),
      subscribe: imageHeaderContainer.$('.page-subscribe')
    };

    api.options = {
      addApp: $('.content-sidebar a[ng-click="$ctrl.addApp($ctrl.page, $ctrl.apps)"]'),
      settings: $('.content-sidebar a[ui-sref="main.page.show.settings"]')
    };

    api.addApp = {
      timeline: $('h5[translate="APP.TIMELINE.NAME"]'),
      save: $('button[form-ctrl="createAppForm"]'),
      back: $('button[ng-click="vm.goBack()"]')
    };

    api.settings = {
      deleteButton: $('a[ng-click="ctrl.delete()"]'),
      deleteButtonConfirm: $('.modal-dialog .btn-primary')
    };
  }

  module.exports = PageDetails;

})();
