(function () {
  'use strict';

  module.exports = {
    sidebar: element(by.css('.sidebar.messaging')),
    onlineStatus: element(by.css('.sidebar.messaging .presence-status .ONLINE')),
    awayStatus: element(by.css('.sidebar.messaging .presence-status .AWAY')),
    presenceStatusView: {
      parent: element(by.css('.messaging-presence-status')),
      close: element(by.css('.messaging-presence-status .messaging-header-icon-bar a')),
      onlineState: element(by.css('.messaging-presence-status li.ONLINE')),
      awayState: element(by.css('.messaging-presence-status li.AWAY'))
    },
    channelsView: {
      parent: element(by.css('.messaging-channels')),
      createChannel: element(by.css('.messaging-channels .messaging-header .zmdi-plus-circle-o')),
      channels: element.all(by.css('.messaging-channels-ch'))
    },
    channelView: {
      parent: element(by.css('.messaging-channel')),
      messages: element.all(by.css('.messaging-channel .message')),
      content: $('.messaging-content'),
      contentContains: function (text) {
        return element(by.cssContainingText('.messaging-content', text));
      },
      newMessage: element(by.css('.messaging-channel textarea.form-control')),
      submit: $('button[ng-click="$ctrl.submitMessage($event)"]')
    },
    createChannelView: {
      parent: element(by.css('.messaging-channels')),
      subject: element(by.model('$ctrl.channel.name')),
      findUser: element(by.model('$ctrl.userSearchTerm')),
      addUser: $('a[ng-click="$ctrl.showUserOptions()"]'),
      searchedUser: element.all(by.repeater('user in $ctrl.userOptions')),
      userItem: element.all(by.css('.messaging-channel-form-user-item')),
      submit: element(by.css('.messaging-channel-form .btn.btn-dark.btn-block')),
      createGroupChannel: $('.messaging-channel-form .zmdi-accounts-add')
    }
  };

})();
